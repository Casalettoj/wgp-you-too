use thiserror::Error;

mod wgpu_adapter;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error, PartialEq)]
pub enum Error {
    #[error("Could not find a suitable adapter.")]
    NoSuitableAdapter,
    #[error("Could not find a suitable format to use for swap chain.")]
    NoSuitableFormat,
    #[error("{0}")]
    RequestDeviceError(String),
    #[error("Swap chain was lost.")]
    SwapChainLost,
    #[error("Swap chain out of memory.")]
    SwapChainOom,
    #[error("Swap chain timed out.")]
    SwapChainTimeout,
    #[error("Swap chain was outdated.")]
    SwapChainOutdated,
}
