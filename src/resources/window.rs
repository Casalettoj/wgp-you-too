use winit::{event_loop::EventLoop, window::Window};

pub mod error;
use error::{Error, Result};

pub struct EventWindow<T: 'static> {
    pub event_loop: EventLoop<T>,
    pub window: Window,
}

// TODO - Options (resizable, sizes, etc)
impl<T: 'static> EventWindow<T> {
    pub fn try_new() -> Result<Self> {
        let event_loop = EventLoop::<T>::with_user_event();
        let window = Window::new(&event_loop)?;
        Ok(Self { event_loop, window })
    }
}
