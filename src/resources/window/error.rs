use thiserror::Error;

mod winit_adapter;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error, PartialEq)]
pub enum Error {
    #[error("{0}")]
    CreationError(String),
}
