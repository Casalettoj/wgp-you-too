use winit::error::OsError;

impl From<OsError> for super::Error {
    fn from(o: OsError) -> Self {
        super::Error::CreationError(o.to_string())
    }
}
