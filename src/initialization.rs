use crate::resources::{pipeline_state::PipelineState, window::EventWindow};
use wgpu::{Features, Limits};

pub mod error;
use error::{Error, Result};

pub struct InitializationOutput<T: 'static> {
    pub window: EventWindow<T>,
    pub pipeline: PipelineState,
}

pub async fn try_initialize_all<T: 'static>(
    required_features: Option<Features>,
    device_limits: Option<Limits>,
) -> Result<InitializationOutput<T>> {
    let window = EventWindow::<T>::try_new()?;
    let pipeline = PipelineState::try_new(&window.window, required_features, device_limits).await?;
    Ok(InitializationOutput { window, pipeline })
}
