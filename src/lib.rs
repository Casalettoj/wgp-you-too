mod initialization;
mod resources;

pub use initialization::try_initialize_all;

pub use resources::{
    pipeline_state::{
        error::{Error as PipelineStateError, Result as PipelineStateResult},
        PipelineState,
    },
    window::{
        error::{Error as WindowError, Result as WindowResult},
        EventWindow,
    },
}; // TODO scope
