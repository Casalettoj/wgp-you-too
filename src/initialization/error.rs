use thiserror::Error;

mod resources_adapter;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Error, PartialEq)]
pub enum Error {
    #[error(transparent)]
    ResourceError(#[from] crate::resources::error::Error),
}
