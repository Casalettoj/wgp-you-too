use crate::resources::{
    pipeline_state::error::Error as PipelineStateError, window::error::Error as WindowResourceError,
};

impl From<PipelineStateError> for super::Error {
    fn from(p: PipelineStateError) -> Self {
        crate::resources::error::Error::from(p).into()
    }
}

impl From<WindowResourceError> for super::Error {
    fn from(w: WindowResourceError) -> Self {
        crate::resources::error::Error::from(w).into()
    }
}
