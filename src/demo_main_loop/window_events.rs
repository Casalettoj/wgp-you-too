use wgp_you_too::PipelineState;
use winit::{
    event::{ElementState, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::ControlFlow,
};

pub fn window_event_handler<T: 'static>(
    pipeline: &mut PipelineState,
    event: &WindowEvent,
    control_flow: &mut ControlFlow,
) {
    // If the event happened in our window and the graphics pipeline is done with the event
    match event {
        WindowEvent::Resized(physical_size) => pipeline.resize((*physical_size).into()),
        WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
            pipeline.resize((**new_inner_size).into())
        }
        WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
        WindowEvent::KeyboardInput { input, .. } => match input {
            KeyboardInput {
                state: ElementState::Pressed,
                virtual_keycode: Some(VirtualKeyCode::Escape),
                ..
            } => *control_flow = ControlFlow::Exit,
            _ => {}
        },
        _ => {}
    }
}
