use wgp_you_too::{EventWindow, PipelineState};
use winit::{event::Event, event_loop::ControlFlow};

mod window_events;

pub fn run_main_loop<T: 'static>(event_window: EventWindow<T>, mut pipeline: PipelineState) -> ! {
    let id = event_window.window.id().clone();
    event_window
        .event_loop
        .run(move |event, _, control_flow| match event {
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == id => {
                if !pipeline.input(event) {
                    window_events::window_event_handler::<T>(&mut pipeline, event, control_flow)
                }
            }
            Event::RedrawRequested(window_id) if window_id == id => {
                pipeline.update();
                match pipeline.render() {
                    Ok(_) => {}
                    Err(wgp_you_too::PipelineStateError::SwapChainLost) => {
                        pipeline.resize(pipeline.size())
                    }
                    Err(wgp_you_too::PipelineStateError::SwapChainOom) => {
                        *control_flow = ControlFlow::Exit
                    }
                    Err(e) => eprintln!("{}", e),
                }
            }
            _ => {}
        });
}
